//
//  FilesHandler.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation

class FilesHandler : NSObject {
    
    public static func getDocumentsDirectory () -> URL {
        do {
            return try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        } catch {
            return URL(fileURLWithPath: "")
        }
    }
    
    public static func writeFile(filename: String, folder: String?, data: Data) -> Bool {
        
        do {
            
            var documentsDirectory = getDocumentsDirectory()
            
            var path = filename
            if (folder != nil) {
                path = folder! + "/" + filename
            }

            documentsDirectory.appendPathComponent(path)
            
            try FileManager.default.createDirectory(at: documentsDirectory.deletingLastPathComponent(),
                                                               withIntermediateDirectories: true,
                                                               attributes: nil)
            try data.write(to: documentsDirectory)
                        
            return true
            
        } catch {
            print("-- Error: \(error)")
            return false
        }
    }
    
    public static func delete (folder : String) -> Bool {
        
        var documentsDirectory = getDocumentsDirectory()
        
        let path = folder

        documentsDirectory.appendPathComponent(path)
        
       do {
            try FileManager.default.removeItem(atPath: documentsDirectory.path)
            return true
        } catch {
            print("-- Error: \(error)")
            return false
        }
    }
    
    public static func delete (filename: String, folder : String?) -> Bool {
        
        var documentsDirectory = getDocumentsDirectory()
        
        var path = filename
        if (folder != nil) {
            path = folder! + "/" + filename
        }

        documentsDirectory.appendPathComponent(path)
        
       do {
            try FileManager.default.removeItem(atPath: documentsDirectory.path)
            return true
        } catch {
            print("-- Error: \(error)")
            return false
        }
    }
    
    public static func delete (path : String) -> Bool {
        
       do {
            try FileManager.default.removeItem(atPath: path)
            return true
        } catch {
            print("-- Error: \(error)")
            return false
        }
    }
    
    public static func delete (url : URL) -> Bool {
        
       do {
            try FileManager.default.removeItem(atPath: url.path)
            return true
        } catch {
            print("-- Error: \(error)")
            return false
        }
    }
    
    public static func exist(filename: String, folder : String?) -> Bool {
        
        var documentsDirectory = getDocumentsDirectory()
        
        var path = filename
        if (folder != nil) {
            path = folder! + "/" + filename
        }

        documentsDirectory.appendPathComponent(path)
        
        return FileManager.default.fileExists(atPath: documentsDirectory.path)
    }

    public static func readFileFromBundle(filename: String, bundle : Bundle?) -> Data? {
        
        var _bundle = bundle
        if (bundle == nil) {
            _bundle = Bundle.main
        }
        if let filepath = _bundle!.path(forResource: filename, ofType: "") {
            return FileManager.default.contents(atPath:filepath)
        } else {
           return nil
        }
    }
    
    public static func readFile(path: String) -> Data? {
        
        return FileManager.default.contents(atPath: path)
    }
    
    public static func readFile(fileUrl: URL) -> Data? {
        
        return FileManager.default.contents(atPath: fileUrl.path)
    }
    
    public static func readFile(filename: String, folder: String?) -> Data? {
        
        var documentsDirectory = getDocumentsDirectory()
        
        var path = filename
        if (folder != nil) {
            path = folder! + "/" + filename
        }

        documentsDirectory.appendPathComponent(path)
        
        return FileManager.default.contents(atPath: documentsDirectory.path)
       
    }
    
    public static func writeJson(filename: String, folder: String?, json: Dictionary<String,Any>) -> Bool {
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: [])
            return writeFile(filename: filename, folder: folder, data: data)
        } catch {
            return false
        }
    }
    
    public static func readJson(filename: String, folder: String?) -> Dictionary<String,Any>? {
        
        do {
            let data = readFile(filename: filename, folder: folder)
            return try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String, Any>
        } catch {
           return nil
        }
    }
    
    public static func countElements(folder: String?) -> Int {
        
        do {
            var documentsDirectory = getDocumentsDirectory()
            
            if (folder != nil) {
                documentsDirectory.appendPathComponent(folder!)
            }

            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil)
                        
            return fileURLs.count
        } catch {
            print("[FilesHandler] -- Error: \(error)")
            return 0
        }
    }
    
    public static func getFileUrls (folder: String?) -> NSMutableArray {
            
        let files = NSMutableArray()
    
        do {
            var documentsDirectory = getDocumentsDirectory()
            
            if (folder != nil) {
                documentsDirectory.appendPathComponent(folder!)
            }
            
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil)

            files.addObjects(from: fileURLs)
            
        } catch {
            print("[FilesHandler] -- Error: \(error)")
        }
            
        return files
     }
    
    public static func getFilesUrls (folder: String?) -> NSMutableArray {
            
        let files = NSMutableArray()
    
        do {
            var documentsDirectory = getDocumentsDirectory()
            
            if (folder != nil) {
                documentsDirectory.appendPathComponent(folder!)
            }
            
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil)

            for url in fileURLs {
                let data = self.readFile(fileUrl: url)
                guard data != nil else { continue}
                files.add(data!)
            }
        } catch {
            print("[FilesHandler] -- Error: \(error)")
        }
            
        return files
     }
    
    public static func sizeOf (folder: String?) -> (size : Int, string : String) {
        
        var documentsDirectory = getDocumentsDirectory()
        
        if (folder != nil) {
            documentsDirectory.appendPathComponent(folder!)
        }

        // check if the url is a directory
        if (try? documentsDirectory.resourceValues(forKeys: [.isDirectoryKey]))?.isDirectory == true {
            var folderSize = 0
            (FileManager.default.enumerator(at: documentsDirectory, includingPropertiesForKeys: nil)?.allObjects as? [URL])?.lazy.forEach {
                folderSize += (try? $0.resourceValues(forKeys: [.totalFileAllocatedSizeKey]))?.totalFileAllocatedSize ?? 0
            }
            let  byteCountFormatter =  ByteCountFormatter()
            byteCountFormatter.allowedUnits = [.useKB] // optional: restricts the units to MB only
            byteCountFormatter.countStyle = .file
            let sizeToDisplay = byteCountFormatter.string(for: folderSize) ?? ""
            return (folderSize, sizeToDisplay)
        }
        
        return (0,"")
    }
    
    public static func sizeOf (filename: String, folder: String?) -> (size : Int, string : String) {
        
        var documentsDirectory = getDocumentsDirectory()
        
        var path = filename
        if (folder != nil) {
            path = folder! + "/" + filename
        }
        
        documentsDirectory.appendPathComponent(path)

        let fileSize = (try? documentsDirectory.resourceValues(forKeys: [.totalFileAllocatedSizeKey]))?.totalFileAllocatedSize ?? 0

        let  byteCountFormatter =  ByteCountFormatter()
        byteCountFormatter.allowedUnits = [.useKB] // optional: restricts the units to MB only
        byteCountFormatter.countStyle = .file
        let sizeToDisplay = byteCountFormatter.string(for: fileSize) ?? ""
        return (fileSize, sizeToDisplay)
        
    }
    
}
