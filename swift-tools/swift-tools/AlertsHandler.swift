//
//  AlertsHandler.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func presentSimpleAlert(withTitle: String, text: String, buttonText: String, preferredStyle: UIAlertController.Style = .alert, anchor : UIView? = nil) {
        
        let alert = UIAlertController(title: withTitle, message: text, preferredStyle: preferredStyle)
        
        alert.addAction(UIAlertAction(title: buttonText, style: .cancel, handler: { action in

        }))
        
        if (anchor != nil && alert.popoverPresentationController != nil) {
                   alert.popoverPresentationController?.sourceView = self.view
                   alert.popoverPresentationController?.sourceRect = anchor!.frame
                   alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
               }
        
        self.present(alert, animated: true)
    }
    
    func presentBoolAlert(withTitle: String, text: String, buttons : (ok: String, ko: String), preferredStyle: UIAlertController.Style = .alert , anchor : UIView? = nil, completion : @escaping (Bool) -> Void) {
        
        let alert = UIAlertController(title: withTitle, message: text, preferredStyle: preferredStyle)
        
        alert.addAction(UIAlertAction(title: buttons.ok, style: .default, handler: { action in
            completion(true)
        }))
        
        alert.addAction(UIAlertAction(title: buttons.ko, style: .cancel, handler: { action in
            completion(false)
        }))
        
        if (anchor != nil && alert.popoverPresentationController != nil) {
                   alert.popoverPresentationController?.sourceView = self.view
                   alert.popoverPresentationController?.sourceRect = anchor!.frame
                   alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
               }
        
        self.present(alert, animated: true)
    }
    
    func presentAlert(withTitle: String, text: String, buttons : Array<String>, preferredStyle: UIAlertController.Style = .alert , anchor : UIView? = nil , completion : @escaping (Int) -> Void) {
        
        let alert = UIAlertController(title: withTitle, message: text, preferredStyle: preferredStyle)
        
        for button in buttons {
            alert.addAction(UIAlertAction(title: button, style: .default, handler: { action in
                completion(buttons.firstIndex(of: button)!)
            }))
        }
        
        if (anchor != nil && alert.popoverPresentationController != nil) {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = anchor!.frame
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        }
        
        self.present(alert, animated: true)
    }
}
