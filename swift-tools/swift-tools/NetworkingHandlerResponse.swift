//
//  NetworkingHandlerResponse.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation

class NetworkingHandlerResponse : NSObject {
    
    init(statusCode : Int, data : Data) {
        
        self.statusCode = statusCode
        self.data = data
        
        do {
            try self.json = JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Any>
        } catch {
            
        }
    }
    
    public let statusCode : Int
    public let data : Data
    public var json : Dictionary<String, Any>? = nil
    
    public func dataInString () -> String? {
        return String(data: data, encoding: .utf8)
    }
    
}
