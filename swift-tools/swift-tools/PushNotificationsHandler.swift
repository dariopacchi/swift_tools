//
//  PushNotificationsHandler.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

//Useful code here
//https://www.raywenderlich.com/8164-push-notifications-tutorial-getting-started

import Foundation
import UserNotifications
import UIKit

class PushNotificationsHandler : NSObject , UNUserNotificationCenterDelegate{
    
    static let shared = PushNotificationsHandler()
    
    private var useBanner = false
    
    public static let PushNotificationName = NSNotification.Name("PushNotification")
    public static let PushNotificationActionName = NSNotification.Name("PushNotificationAction")
    
    public static func register(completed : @escaping(Bool) -> Void) {
        UNUserNotificationCenter.current() // 1
            .requestAuthorization(options: [.alert, .sound, .badge]) { // 2
                granted, error in
                completed (granted)
        }
    }
    
    public static func enable(completed : @escaping(Bool) -> Void) {
        
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            guard settings.authorizationStatus == .authorized else {
                completed(false)
                return
            }
            DispatchQueue.main.async {
                completed(true)
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    public static func setup (useBanner : Bool, defaultIcon: String?) {
        UNUserNotificationCenter.current().delegate = shared
        shared.useBanner = useBanner
        if (defaultIcon != nil) {
            PushNotificationsBanner.defaultImage = UIImage.init(named: defaultIcon!)
        }
    }
    
    public static func localNotification () {
        
        let content = UNMutableNotificationContent()
        content.title = "Weekly Staff Meeting"
        content.body = "Every Tuesday at 2pm"
        
        // Create the trigger as a repeating event.
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
        
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
        
        // Schedule the request with the system.
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(request) { (error) in
            if error != nil {
                // Handle any errors.
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        
        //Called when a notification is delivered to a foreground app.
        let userInfo = notification.request.content.userInfo as NSDictionary
        guard let aps = userInfo["aps"] as? [String: AnyObject] else {
            return
        }
        
        printLog(text: aps)
        
        if (useBanner == true) {
            let _ = PushNotificationsBanner.presentBanner(title: aps["alert"]!["title"] as? String, subtitle: aps["alert"]!["body"] as? String, icon: nil)
        }
        
        NotificationCenter.default.post(name: PushNotificationsHandler.PushNotificationName, object: nil, userInfo: (userInfo as! [AnyHashable : Any]))
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // Called to let your app know which action was selected by the user for a given notification.
        let userInfo = response.notification.request.content.userInfo as NSDictionary
        guard let aps = userInfo["aps"] as? [String: AnyObject] else {
            return
        }
        
        printLog(text: aps)
        NotificationCenter.default.post(name: PushNotificationsHandler.PushNotificationActionName, object: nil, userInfo: (userInfo as! [AnyHashable : Any]))
    }
    
    
    //MARK: - Copy those methods in the app delegate file
    
    //    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //      let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
    //      let token = tokenParts.joined()
    //      print("Device Token: \(token)")
    //    }
    //
    //    func application(_ application: UIApplication,didFailToRegisterForRemoteNotificationsWithError error: Error) {
    //      print("Failed to register: \(error)")
    //    }
    //
    //    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    //        guard let aps = userInfo["aps"] as? [String: AnyObject] else {
    //            completionHandler(.failed)
    //        return
    //      }
    //    }
    
    //MARK: - Firebase only
    
    //  FirebaseApp.configure()
    //  Messaging.messaging().delegate = self
    
    //    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    //        print("Firebase registration token: \(fcmToken)")
    //
    //        let dataDict:[String: String] = ["token": fcmToken]
    //        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    //        // TODO: If necessary send token to application server.
    //        // Note: This callback is fired at each app startup and whenever a new token is generated.
    //      }
}
