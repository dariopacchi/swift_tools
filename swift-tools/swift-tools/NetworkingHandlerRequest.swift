//
//  NetworkingHandlerRequest.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation

public class NetworkingHandlerRequest {
    
    var url: String
    var filename : String?
    var data : Data?
    var mimeType : String?
    var params: [String: Any]?
    
    var username : String?
    var password : String?
    
    init(url : String) {
        self.url = url
    }
    
    init (url : String, username : String, password: String) {
        self.url = url
        self.username = username
        self.password = password
    }
    
    init(url : String, params: [String: Any]) {
        self.url = url
        self.params = params
    }
    
    init(url : String, filename : String, data : Data, mimeType: String, params: [String: Any]) {
        self.url = url
        self.filename = filename
        self.data = data
        self.mimeType = mimeType
        self.params = params
    }
    
    public func body () -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: params as Any, options: [])
        } catch {
            return nil
        }
    }
    
    public func baseAuthenticationHeader () -> String {

        let loginString = String(format: "%@:%@", username!, password!)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()

        return base64LoginString
    }

}
