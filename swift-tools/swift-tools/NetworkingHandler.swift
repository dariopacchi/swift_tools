//
//  NetworkingHandler.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation
import UIKit

class NetworkingHandler : NSObject {
    
    private static func makeResponse(withData : Data, statusCode: Int) -> NetworkingHandlerResponse{
        return NetworkingHandlerResponse.init(statusCode: statusCode, data: withData)
    }
    
    public static func get(handlerRequest : NetworkingHandlerRequest, onCompletion: @escaping (NetworkingHandlerResponse?, Error?) -> Void) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let url = URL(string: handlerRequest.url)!
            
            let request = NSMutableURLRequest(url:url as URL);
            request.httpMethod = "GET"
            
            self.performNURLRequest(request: request, onCompletion: onCompletion)

        }
    }
    
    public static func post(handlerRequest : NetworkingHandlerRequest, onCompletion: @escaping (NetworkingHandlerResponse?, Error?) -> Void) {
        
        guard handlerRequest.params != nil else {
            onCompletion(nil,nil)
            return
        }
        
        let body = handlerRequest.body()
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let url = URL(string: handlerRequest.url)!
            
            let request = NSMutableURLRequest(url:url as URL);
            request.httpMethod = "POST"
            
//            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            
            request.httpBody = body!
            
            self.performNURLRequest(request: request, onCompletion: onCompletion)

        }
    }
    
    public static func multipartForm(handlerRequest : NetworkingHandlerRequest, onCompletion: @escaping (NetworkingHandlerResponse?, Error?) -> Void) {
        
        guard handlerRequest.data != nil else {
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let url = URL(string: handlerRequest.url)!
            
            let params = handlerRequest.params!
            
            let request = NSMutableURLRequest(url:url as URL);
            request.httpMethod = "POST"
            
            let boundary = generateBoundaryString()
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            request.httpBody = createBodyWithParameters(parameters: params, fileName: handlerRequest.filename!, mimeType: handlerRequest.mimeType! ,filePathKey: "file", fileData: handlerRequest.data! as NSData, boundary: boundary) as Data
            
            self.performNURLRequest(request: request, onCompletion: onCompletion)

        }
    }
    
    
    public static func createBodyWithParameters(parameters: [String: Any]?, fileName: String, mimeType : String,filePathKey: String?, fileData: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        //            let mimetype = "application/zip"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(fileName)\"\r\n")
        body.appendString(string: "Content-Type: \(mimeType)\r\n\r\n")
        body.append(fileData as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    public static func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    public static func basicAuthentication(handlerRequest : NetworkingHandlerRequest, onCompletion: @escaping (NetworkingHandlerResponse?, Error?) -> Void) {
        
        guard handlerRequest.params != nil else {
            onCompletion(nil,nil)
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let url = URL(string: handlerRequest.url)!
            
            let request = NSMutableURLRequest(url:url as URL);
            request.httpMethod = "POST"
            
            request.setValue("Basic \(handlerRequest.baseAuthenticationHeader())", forHTTPHeaderField: "Authorization")
            
            self.performNURLRequest(request: request, onCompletion: onCompletion)
        }
    }
    
    private static func performNURLRequest (request : NSURLRequest, onCompletion : @escaping (NetworkingHandlerResponse?, Error?) -> Void){
        
        let task =  URLSession.shared.dataTask(with: request as URLRequest,
                                               completionHandler: {
                                                (data, response, error) -> Void in
                                                
                                                guard let data = data,
                                                    let response = response as? HTTPURLResponse,
                                                    error == nil else {
                                                        print("[NetworkingHandler] - Generic network error: \(error.debugDescription)")
                                                        DispatchQueue.main.async() {
                                                            onCompletion(nil,error)
                                                        }
                                                        return
                                                }
                                                
                                                guard (200 ... 299) ~= response.statusCode else {
                                                    print("[NetworkingHandler] - Http error: \(error.debugDescription)")
                                                    DispatchQueue.main.async() {
                                                        onCompletion(nil,error)
                                                    }
                                                    return
                                                }
                                                
                                                DispatchQueue.main.async() {
                                                    onCompletion(makeResponse(withData: data, statusCode: response.statusCode),nil)
                                                }
        })
        task.resume()
    }
    
    
    public static func checkNOTJailbrokenDevice () -> Bool {
        
        if TARGET_IPHONE_SIMULATOR != 1 {
            // Check 1 : existence of files that are common for jailbroken devices
            if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
                || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
                || FileManager.default.fileExists(atPath: "/bin/bash")
                || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
                || FileManager.default.fileExists(atPath: "/etc/apt")
                || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
                || UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!)
            {
                return true
            }
            
            // Check 2 : Reading and writing in system directories (sandbox violation)
            let stringToWrite = "Jailbreak Test"
            do {
                try stringToWrite.write(toFile:"/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
                //Device is jailbroken
                return true
            } catch {

            }
        }
        
        return false
    }
    
    public static func openUrlWithSafari (url : String) -> Bool {
        
        guard let url = URL(string: url) else { return false}
        
        if (UIApplication.shared.canOpenURL(url)) {
            UIApplication.shared.open(url)
            return true
        }
        
        return false
    }
        
}

extension NSObject {
    
    func printLog(text : Any?) {
        print("[\(String(describing: self))] - \(text)")
    }
}

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}


