//
//  PushNotificationsBanner.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 25/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation
import UIKit

class PushNotificationsBanner : UIView {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var subtitle: UILabel!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var topConstraint : NSLayoutConstraint!
    static var defaultImage : UIImage?
    static let bannerAnimationTime = 0.3
        
    private var onTap : (() -> Void)?
    
    required init?(coder aDecoder: NSCoder) {   // 2 - storyboard initializer
       super.init(coder: aDecoder)
    }
    
    static func presentBanner(title:String?,subtitle:String?,icon:UIImage?,circle : Bool = false, timer:Double = 2) -> PushNotificationsBanner{
        
        let banner = PushNotificationsBanner.load(title: title, subtitle: subtitle, icon: icon, circle: circle, timer: timer)

       return banner
    }
    
    static func presentBanner(title:String?,subtitle:String?,icon:UIImage?,circle : Bool = false, timer:Double = 2, onTap: (() -> Void)?) -> PushNotificationsBanner{
        
        let banner = PushNotificationsBanner.load(title: title, subtitle: subtitle, icon: icon, circle: circle, timer: timer)

        if (onTap != nil) {
            banner.onTap = onTap
            let tapRecognizer = UITapGestureRecognizer.init(target: banner, action: #selector(banner.tapped))
            banner.contentView.addGestureRecognizer(tapRecognizer)
        }
        
       return banner
    }
    
    private static func load(title:String?,subtitle:String?,icon:UIImage?,circle : Bool = false, timer:Double = 2) -> PushNotificationsBanner{
        
        let banner : PushNotificationsBanner = UIView.fromNib()
        banner.title.text = title
        banner.subtitle.text = subtitle
        
        if (banner.imageView.image == nil){
           banner.imageView.image = defaultImage
        } else {
            banner.imageView.image = icon
        }
        
        banner.constraintToWindow()
        
        banner.topConstraint.constant = -200
        banner.setNeedsLayout()
        banner.layoutIfNeeded()
        
        banner.animateView()
        banner.animateBack(after: timer)
        
        banner.imageView.layer.cornerRadius = circle == true ? banner.frame.size.width : 8

       return banner
    }
    
    
    //MARK: - Animation
    func animateView() {
        UIView.animate(withDuration: PushNotificationsBanner.bannerAnimationTime) {[weak self] in
            self?.topConstraint?.constant = 8
            self?.setNeedsLayout()
            self?.layoutIfNeeded()
        }
    }
    
    func animateBack(after:Double) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + after) {
            UIView.animate(withDuration: PushNotificationsBanner.bannerAnimationTime, animations: {[weak self] in
                 self?.topConstraint?.constant = -200
                 self?.setNeedsLayout()
                 self?.layoutIfNeeded()
            }) {[weak self] (success)  in
                self?.removeFromSuperview()
            }
       }
    }
    
    
    //MARK: - Gesture
    @objc func tapped(_ sender: UITapGestureRecognizer) {
        onTap!()
    }
}
