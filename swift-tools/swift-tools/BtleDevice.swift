//
//  BtleDevice.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 10/03/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation
import CoreBluetooth

class BtleDevice {
    
    let deviceName : String
    let readCharacteristicID : String
    let writeCharacteristicID : String
    let onValueChange : (([UInt8], CBCharacteristic) -> Void)?
    let autoconnect : Bool
    
    public var sendWhenConnected : [UInt8]?
    
    public var connected : Bool = false
    public var peripheral : CBPeripheral?

    init (deviceName: String, readCharacteristic: String, writeCharacteristic: String, autoconnect : Bool = true ,onValueChange : (([UInt8] , CBCharacteristic) -> Void)?) {
        self.deviceName = deviceName
        self.readCharacteristicID = readCharacteristic
        self.writeCharacteristicID = writeCharacteristic
        self.onValueChange = onValueChange
        self.autoconnect = autoconnect
    }

    public func sendValue (value : [UInt8]) {
        
    }

}
