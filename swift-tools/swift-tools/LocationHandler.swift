//
//  LocationHandler.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation
import CoreLocation
public class LocationHandler : NSObject , CLLocationManagerDelegate {
    
    static let shared = LocationHandler()

    public var locationManager : CLLocationManager?
    public var isAskingForLocalizationPermission = false

    public var onPermissionChange : ((Bool) -> Void)?
    public var onLocationChange : ((CLLocation) -> Void)?
    public var onHeadingChange : ((CLHeading) -> Void)?

    override init(){
        
    }
    
    public func setOnPermissionChange (closure: @escaping (Bool) -> Void) {
        onPermissionChange = closure
    }
    public func setOnLocationChange (closure: @escaping (CLLocation) -> Void) {
        onLocationChange = closure
    }
    public func setOnHeadingChange (closure: @escaping (CLHeading) -> Void) {
        onHeadingChange = closure
    }
    
    //MARK: GPS
    public func requestGPS () {
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        isAskingForLocalizationPermission = true
    }
    
    public func enableGps () {
        
        locationManager?.startUpdatingHeading()
        locationManager?.startUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
       
        isAskingForLocalizationPermission = false
        DispatchQueue.main.async() {[weak self] in
            guard self?.onPermissionChange == nil else {
                if status == .authorizedAlways || status == .authorizedWhenInUse {
                    self?.onPermissionChange!(true)
                }
                else {
                    self?.onPermissionChange!(false)
                }
                return
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        DispatchQueue.main.async() {[weak self] in
            guard self?.onHeadingChange == nil else {
                self?.onHeadingChange!(newHeading)
                return
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DispatchQueue.main.async() {[weak self] in
            guard self?.onLocationChange == nil else {
                if (locations.count > 0){
                    self?.onLocationChange!(locations.first!)
                }
                return
            }
        }
    }
    
}
