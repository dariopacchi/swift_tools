//
//  scenekit_extensions.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 13/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import SceneKit.ModelIO

extension UIColor {
    class var arBlue: UIColor {
        get {
            return UIColor(red: 0.141, green: 0.540, blue: 0.816, alpha: 1)
        }
    }
}

extension ARSession {
    func run() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        configuration.isLightEstimationEnabled = true
        run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
}

extension SCNNode {
    
    class func sphereNode(color: UIColor, radius: Float) -> SCNNode {
        let geometry = SCNSphere(radius: CGFloat(radius))
        geometry.materials.first?.diffuse.contents = color
        return SCNNode(geometry: geometry)
    }
    
    class func textNode(text: String) -> SCNNode {
        let geometry = SCNText(string: text, extrusionDepth: 0.01)
        geometry.alignmentMode = convertFromCATextLayerAlignmentMode(CATextLayerAlignmentMode.center)
        if let material = geometry.firstMaterial {
            material.diffuse.contents = UIColor.white
            material.isDoubleSided = true
        }
        let textNode = SCNNode(geometry: geometry)

        geometry.font = UIFont.systemFont(ofSize: 1)
        textNode.scale = SCNVector3Make(0.02, 0.02, 0.02)

        // Translate so that the text node can be seen
        let (min, max) = geometry.boundingBox
        textNode.pivot = SCNMatrix4MakeTranslation((max.x - min.x)/2, min.y - 0.5, 0)
        
        // Always look at the camera
        let node = SCNNode()
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        node.constraints = [billboardConstraint]

        node.addChildNode(textNode)
        
        return node
    }
    
    class func lineNode(length: CGFloat, color: UIColor) -> SCNNode {
        let geometry = SCNCapsule(capRadius: 0.008, height: length)
        geometry.materials.first?.diffuse.contents = color
        let line = SCNNode(geometry: geometry)
        
        let node = SCNNode()
        node.eulerAngles = SCNVector3Make(Float.pi/2, 0, 0)
        node.addChildNode(line)
        
        return node
    }

    func loadScn(name: String, inDirectory directory: String) {
        guard let scene = SCNScene(named: "\(name).scn", inDirectory: directory) else { fatalError() }
        for child in scene.rootNode.childNodes {
            child.geometry?.firstMaterial?.lightingModel = .physicallyBased
            addChildNode(child)
        }
    }
    
    func loadUsdz(name: String) {
        guard let url = Bundle.main.url(forResource: name, withExtension: "usdz") else { fatalError() }
        let mdlAsset = MDLAsset(url: url)
        let scene = SCNScene(mdlAsset: mdlAsset)
        for child in scene.rootNode.childNodes {
            child.geometry?.firstMaterial?.lightingModel = .physicallyBased
            addChildNode(child)
        }
    }
}

extension SCNView {
    
    private func enableEnvironmentMapWithIntensity(_ intensity: CGFloat) {
        if scene?.lightingEnvironment.contents == nil {
            if let environmentMap = UIImage(named: "models.scnassets/sharedImages/environment_blur.exr") {
                scene?.lightingEnvironment.contents = environmentMap
            }
        }
        scene?.lightingEnvironment.intensity = intensity
    }

    func updateLightingEnvironment(for frame: ARFrame) {
        // If light estimation is enabled, update the intensity of the model's lights and the environment map
        let intensity: CGFloat
        if let lightEstimate = frame.lightEstimate {
            intensity = lightEstimate.ambientIntensity / 400
        } else {
            intensity = 2
        }
        DispatchQueue.main.async(execute: {
            self.enableEnvironmentMapWithIntensity(intensity)
        })
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromCATextLayerAlignmentMode(_ input: CATextLayerAlignmentMode) -> String {
    return input.rawValue
}

public func rad2deg(_ number: Float) -> Float {
    return number * 180 / .pi
}

public func animateColor(from: UIColor, to: UIColor, percentage: CGFloat) -> UIColor {
    let fromComponents = from.cgColor.components!
    let toComponents = to.cgColor.components!
    
    let color = UIColor(red: fromComponents[0] + (toComponents[0] - fromComponents[0]) * percentage,
                        green: fromComponents[1] + (toComponents[1] - fromComponents[1]) * percentage,
                        blue: fromComponents[2] + (toComponents[2] - fromComponents[2]) * percentage,
                        alpha: fromComponents[3] + (toComponents[3] - fromComponents[3]) * percentage)
    return color
}

public func animateMaterial(from: SCNMaterial, to: SCNMaterial, percentage: CGFloat) -> SCNMaterial {
    let fromComponents = (from.diffuse.contents as! UIColor).cgColor.components!
    let toComponents = (to.diffuse.contents as! UIColor).cgColor.components!
    
    let color = UIColor(red: fromComponents[0] + (toComponents[0] - fromComponents[0]) * percentage,
                        green: fromComponents[1] + (toComponents[1] - fromComponents[1]) * percentage,
                        blue: fromComponents[2] + (toComponents[2] - fromComponents[2]) * percentage,
                        alpha: fromComponents[3] + (toComponents[3] - fromComponents[3]) * percentage)
    let mat = SCNMaterial()
    mat.diffuse.contents = color
    return mat
}

func SCNDirection (a:SCNVector3, b:SCNVector3) -> SCNVector3 {
    
    return a - b
}

func SCNPerpendicular (a:SCNVector3, b:SCNVector3) -> SCNVector3 {
    
    return SCNDirection(a: a, b: b).cross(vector: SCNVector3.init(0, 1, 0))
}

func + (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x + right.x, left.y + right.y, left.z + right.z)
}

func += (left: inout SCNVector3, right: SCNVector3) {
    left = left + right
}

func - (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x - right.x, left.y - right.y, left.z - right.z)
}

func * (vector: SCNVector3, scalar: Float) -> SCNVector3 {
    return SCNVector3Make(vector.x * scalar, vector.y * scalar, vector.z * scalar)
}

func / (left: SCNVector3, right: Float) -> SCNVector3 {
    return SCNVector3Make(left.x / right, left.y / right, left.z / right)
}

func /= (left: inout SCNVector3, right: Float) {
    left = left / right
}

extension SCNVector3 {
    func length() -> Float {
        return sqrtf(x * x + y * y + z * z)
    }
}

extension matrix_float4x4 {
    func position() -> SCNVector3 {
        let mat = SCNMatrix4(self)
        return SCNVector3(mat.m41, mat.m42, mat.m43)
    }
}

extension SCNVector3
{
    /**
     * Negates the vector described by SCNVector3 and returns
     * the result as a new SCNVector3.
     */
    func negate() -> SCNVector3 {
        return self * -1
    }
    
    /**
     * Negates the vector described by SCNVector3
     */
    mutating func negated() -> SCNVector3 {
        self = negate()
        return self
    }
    
    /**
     * Returns the length (magnitude) of the vector described by the SCNVector3
     */
//    func length() -> Float {
//        return sqrtf(x*x + y*y + z*z)
//    }
    
    /**
     * Normalizes the vector described by the SCNVector3 to length 1.0 and returns
     * the result as a new SCNVector3.
     */
    func normalized() -> SCNVector3 {
        return self / length()
    }
    
    func flattened() -> SCNVector3 {
        
        return SCNVector3Make(x, 0, y)
    }
    
    /**
     * Normalizes the vector described by the SCNVector3 to length 1.0.
     */
    mutating func normalize() -> SCNVector3 {
        self = normalized()
        return self
    }
    
    /**
     * Calculates the distance between two SCNVector3. Pythagoras!
     */
    func distance(vector: SCNVector3) -> Float {
        return (self - vector).length()
    }
    
    /**
     * Calculates the distance between two SCNVector3. Pythagoras!
     */
    func flatDistance(vector: SCNVector3) -> Float {
        return (SCNVector3(self.x, 0, self.z) - SCNVector3(vector.x,0,vector.z)).length()
    }
    
    /**
     * Calculates the dot product between two SCNVector3.
     */
    func dot(vector: SCNVector3) -> Float {
        return x * vector.x + y * vector.y + z * vector.z
    }
    
    /**
     * Calculates the cross product between two SCNVector3.
     */
    func cross(vector: SCNVector3) -> SCNVector3 {
        return SCNVector3Make(y * vector.z - z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x)
    }
    
    func divide ( scalar: Float) -> SCNVector3 {
        return SCNVector3Make(x / scalar, y / scalar, z / scalar)
    }
    
    func multiply (scalar: Float) -> SCNVector3 {
        return SCNVector3Make(x * scalar, y * scalar, z * scalar)
    }
    
    func projectOnALine(vA : SCNVector3, vB : SCNVector3) -> SCNVector3
    {
//        let vA = SCNVector3Make(vA.x, self.y, vA.z)
//        let vB = SCNVector3Make(vB.x, self.y, vB.z)

        let vVector1 = self - vA
        let vVector2 = (vB - vA).normalized
        
        let d = vA.distance(vector: vB)
        let t = vVector2().dot(vector: vVector1)
        
        if (t <= 0){
            return vA;
        }
        
        if (t >= d) {
            return vB;
        }
        
        let vVector3 = vVector2().multiply(scalar: t);
        
        let vClosestPoint = vA + vVector3;
        
        return vClosestPoint;
    }
    
    /**
     Returns lerp from Vector to Vector
     */
    func lerp(toVector: SCNVector3, t: Float) -> SCNVector3 {
        return SCNVector3Make(
            self.x + ((toVector.x - self.x) * t),
            self.y + ((toVector.y - self.y) * t),
            self.z + ((toVector.z - self.z) * t))
    }
    
    /// Calculate the magnitude of this vector
    var magnitude:SCNFloat {
        get {
            return sqrt(dotProduct(self))
        }
    }

    /**
     Calculate the dot product of two vectors
     
     - parameter vectorB: Other vector in the calculation
     */
    func dotProduct(_ vectorB:SCNVector3) -> SCNFloat {
        
        return (x * vectorB.x) + (y * vectorB.y) + (z * vectorB.z)
    }
    
    func angleBetweenVectors(_ vectorB:SCNVector3) -> SCNFloat {
        
        //cos(angle) = (A.B)/(|A||B|)
        let cosineAngle = (dotProduct(vectorB) / (magnitude * vectorB.magnitude))
        return SCNFloat(acos(cosineAngle))
    }
    
    func angleBetweenVectorsInDegrees(_ vectorB:SCNVector3) -> SCNFloat {
        
        //cos(angle) = (A.B)/(|A||B|)
        let cosineAngle = (dotProduct(vectorB) / (magnitude * vectorB.magnitude))
        return  rad2deg(SCNFloat(acos(cosineAngle)))
    }
    
    func angleBetweenVectors2DInDegrees(_ vectorB:SCNVector3) -> SCNFloat {
        
        //cos(angle) = (A.B)/(|A||B|)
        
        let v1 = self.flattened()
        let v2 = vectorB.flattened()
        
        let cosineAngle = (v1.dotProduct(v2.flattened()) / (magnitude * v2.magnitude))
        return  rad2deg(SCNFloat(acos(cosineAngle)))
    }
    
    func rad2deg(_ number: Float) -> Float {
        return number * 180 / .pi
    }
}

/**
 * Adds two SCNVector3 vectors and returns the result as a new SCNVector3.
 */
//func + (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
//    return SCNVector3Make(left.x + right.x, left.y + right.y, left.z + right.z)
//}

/**
 * Increments a SCNVector3 with the value of another.
 */
//func += (inout left: SCNVector3, right: SCNVector3) {
//    left = left + right
//}

/**
 * Subtracts two SCNVector3 vectors and returns the result as a new SCNVector3.
 */
//func - (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
//    return SCNVector3Make(left.x - right.x, left.y - right.y, left.z - right.z)
//}

/**
 * Decrements a SCNVector3 with the value of another.
 */
//func -= (inout left: SCNVector3, right: SCNVector3) {
//    left = left - right
//}

/**
 * Multiplies two SCNVector3 vectors and returns the result as a new SCNVector3.
 */
func * (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x * right.x, left.y * right.y, left.z * right.z)
}

/**
 * Multiplies a SCNVector3 with another.
 */
func *= ( left: inout SCNVector3, right: SCNVector3) {
    left = left * right
}

/**
 * Multiplies the x, y and z fields of a SCNVector3 with the same scalar value and
 * returns the result as a new SCNVector3.
 */
//func * (vector: SCNVector3, scalar: Float) -> SCNVector3 {
//    return SCNVector3Make(vector.x * scalar, vector.y * scalar, vector.z * scalar)
//}

/**
 * Multiplies the x and y fields of a SCNVector3 with the same scalar value.
 */
//func *= (inout vector: SCNVector3, scalar: Float) {
//    vector = vector * scalar
//}

/**
 * Divides two SCNVector3 vectors abd returns the result as a new SCNVector3
 */
func / (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x / right.x, left.y / right.y, left.z / right.z)
}

/**
 * Divides a SCNVector3 by another.
 */
//func /= (inout left: SCNVector3, right: SCNVector3) {
//    left = left / right
//}

/**
 * Divides the x, y and z fields of a SCNVector3 by the same scalar value and
 * returns the result as a new SCNVector3.
 */
func Divide (vector: SCNVector3, scalar: Float) -> SCNVector3 {
    return SCNVector3Make(vector.x / scalar, vector.y / scalar, vector.z / scalar)
}

/**
 * Divides the x, y and z of a SCNVector3 by the same scalar value.
 */
//func /= (inout vector: SCNVector3, scalar: Float) {
//    vector = vector / scalar
//}

/**
 * Negate a vector
 */
func SCNVector3Negate(vector: SCNVector3) -> SCNVector3 {
    return vector * -1
}

/**
 * Returns the length (magnitude) of the vector described by the SCNVector3
 */
func SCNVector3Length(vector: SCNVector3) -> Float
{
    return sqrtf(vector.x*vector.x + vector.y*vector.y + vector.z*vector.z)
}

/**
 * Returns the distance between two SCNVector3 vectors
 */
//func SCNVector3Distance(vectorStart: SCNVector3, vectorEnd: SCNVector3) -> Float {
//    return SCNVector3Length(vectorEnd - vectorStart)
//}
//
///**
// * Returns the distance between two SCNVector3 vectors
// */
//func SCNVector3Normalize(vector: SCNVector3) -> SCNVector3 {
//    return vector / SCNVector3Length(vector)
//}

/**
 * Calculates the dot product between two SCNVector3 vectors
 */
func SCNVector3DotProduct(left: SCNVector3, right: SCNVector3) -> Float {
    return left.x * right.x + left.y * right.y + left.z * right.z
}

/**
 * Calculates the cross product between two SCNVector3 vectors
 */
func SCNVector3CrossProduct(left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.y * right.z - left.z * right.y, left.z * right.x - left.x * right.z, left.x * right.y - left.y * right.x)
}

/**
 * Calculates the SCNVector from lerping between two SCNVector3 vectors
 */
func SCNVector3Lerp(vectorStart: SCNVector3, vectorEnd: SCNVector3, t: Float) -> SCNVector3 {
    return SCNVector3Make(vectorStart.x + ((vectorEnd.x - vectorStart.x) * t), vectorStart.y + ((vectorEnd.y - vectorStart.y) * t), vectorStart.z + ((vectorEnd.z - vectorStart.z) * t))
}
