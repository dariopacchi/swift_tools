//
//  codable_extensions.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 13/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation

extension Encodable {
    func encoded() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func encodedToString() throws -> String {
        return try (String.init(data: encoded(), encoding: .utf8) ?? "")
    }
    
    func encodeToFile(filename: String, folder: String?) -> Bool {
        do {
            return try FilesHandler.writeFile(filename: filename, folder: folder, data: encoded())
        } catch {
            return false
        }
    }
}

func decodeFromFile<T:Decodable> (url : URL) -> T? {
    let data = FilesHandler.readFile(fileUrl: url)
    guard data != nil else {
        return nil
    }
    do {
         return try data!.decoded() as T
    } catch {
        return nil
    }
}

func decodeFromFile<T:Decodable> (path : String) -> T? {
    let data = FilesHandler.readFile(path: path)
    guard data != nil else {
        return nil
    }
    do {
         return try data!.decoded() as T
    } catch {
        return nil
    }
}

func decodeFromFile<T:Decodable> (filename: String, folder: String?) -> T? {
    let data = FilesHandler.readFile(filename: filename, folder: folder)
    guard data != nil else {
        return nil
    }
    do {
         return try data!.decoded() as T
    } catch {
        return nil
    }
}

extension Data {
    func decoded<T: Decodable>() throws -> T {
        return try JSONDecoder().decode(T.self, from: self)
    }
}
