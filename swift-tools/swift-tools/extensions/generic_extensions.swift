//
//  generic_extensions.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 13/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func circle () {
        self.layer.cornerRadius = self.frame.size.height
    }
    
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    //Drag

    func addDrag(withGravity:Bool = false) {
        DragHolder._gravity = withGravity
        let selector = withGravity == true ? #selector(gravityDrag(_:)) : #selector(panDrag(_:))
        let pan = UIPanGestureRecognizer.init(target: self, action: selector)
        self.addGestureRecognizer(pan)
    }

    struct DragHolder {
        static var _lastCenter:CGPoint = CGPoint.zero
        static var _gravity:Bool = false
    }
    private var lastCenter:CGPoint {
        get {
            return DragHolder._lastCenter
        }
        set(newValue) {
            DragHolder._lastCenter = newValue
        }
    }
    @objc private func panDrag(_ recognizer:UIPanGestureRecognizer) {
        let translation  = recognizer.translation(in: self.superview)
        if (recognizer.state == .began) {
            lastCenter = self.center
            if (animator != nil) {
                animator?.removeAllBehaviors()
            }
        }
        self.center = CGPoint(x:lastCenter.x + translation.x, y: lastCenter.y + translation.y)
        
        if (recognizer.state == .ended && animator != nil) {
            addGravity()
        }
    }
    
    //Gravity
    struct  AnimatorHolder {
        static var _animator:UIDynamicAnimator?
        static var _attachmentBehavior : UIAttachmentBehavior?
        static var _pushBehavior : UIPushBehavior?
        static var _itemBehavior : UIDynamicItemBehavior?
    }
    private var animator:UIDynamicAnimator? {
        get {
            return AnimatorHolder._animator
        }
        set(newValue) {
            AnimatorHolder._animator = newValue
        }
    }
    
    
    
    @objc private func gravityDrag(_ sender:UIPanGestureRecognizer) {
        
        let ThrowingThreshold: CGFloat = 1000
        let ThrowingVelocityPadding: CGFloat = 350
        
        let location = sender.location(in: self.superview)
        let boxLocation = sender.location(in: self)
        
        if (animator == nil) {
            animator = UIDynamicAnimator(referenceView: self.superview!)
        }
        guard let anim = animator else {
            return
        }

        switch sender.state {
        case .began:

          anim.removeAllBehaviors()

          let centerOffset = UIOffset(horizontal: boxLocation.x - self.bounds.midX, vertical: boxLocation.y - self.bounds.midY)
          AnimatorHolder._attachmentBehavior = UIAttachmentBehavior(item: self, offsetFromCenter: centerOffset, attachedToAnchor: location)

          anim.addBehavior(AnimatorHolder._attachmentBehavior!)

        case .ended:

          anim.removeAllBehaviors()

          // 1
          let velocity = sender.velocity(in: self.superview)
          let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y))

          if magnitude > ThrowingThreshold {
            // 2
            AnimatorHolder._pushBehavior = UIPushBehavior(items: [self], mode: .instantaneous)
            AnimatorHolder._pushBehavior!.pushDirection = CGVector(dx: velocity.x / 10, dy: velocity.y / 10)
            AnimatorHolder._pushBehavior!.magnitude = magnitude / ThrowingVelocityPadding

            anim.addBehavior(AnimatorHolder._pushBehavior!)

//            // 3
//            let angle = Int(arc4random_uniform(20)) - 10
//
//            AnimatorHolder._itemBehavior = UIDynamicItemBehavior(items: [self])
//            AnimatorHolder._itemBehavior!.friction = 0.2
//            AnimatorHolder._itemBehavior!.allowsRotation = true
//            AnimatorHolder._itemBehavior!.addAngularVelocity(CGFloat(angle), for: self)
//            anim.addBehavior(AnimatorHolder._itemBehavior!)

            
          }

        default:
            AnimatorHolder._attachmentBehavior!.anchorPoint = sender.location(in: self.superview)
          break
        }
    }
    
    func addGravity () {
        
        // Generate UIDynamiAnimator and save instance.
        if (animator == nil) {
            animator = UIDynamicAnimator(referenceView: self.superview!)
        }
        // Create gravity and apply it to View.
        let gravity = UIGravityBehavior(items: [self])
        
        // Create Collision and apply it to View.
        let collision = UICollisionBehavior(items: [self])
        
        // Specify the behavior of Collision.
        collision.addBoundary(withIdentifier: "barrier" as NSCopying, for: UIBezierPath(rect: self.superview!.frame))
        
        // Run Collision's animation.
        animator!.addBehavior(collision)
        animator!.addBehavior(gravity)
        
    }
    
//    func addInertia () {
//
//        if (animator == nil) {
//            animator = UIDynamicAnimator(referenceView: self.superview!)
//        }
//
//        let snapBehavior = UISnapBehavior(item: self, snapTo: self.center)
//        animator!.addBehavior(snapBehavior!)
//
//    }
    
    func constraintInView(superView : UIView, margin : CGFloat = 0) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        superView.addSubview(self)
        
        let leading = NSLayoutConstraint.init(item: self, attribute: .leading, relatedBy: .equal, toItem: superView, attribute: .leading, multiplier: 1, constant: margin)
        let trailing = NSLayoutConstraint.init(item: self, attribute: .trailing, relatedBy: .equal, toItem: superView, attribute: .trailing, multiplier: 1, constant: -margin)
        let top = NSLayoutConstraint.init(item: self, attribute: .top, relatedBy: .equal, toItem: superView, attribute: .top, multiplier: 1, constant: margin)
        let bottom = NSLayoutConstraint.init(item: self, attribute: .bottom, relatedBy: .equal, toItem: superView, attribute: .bottom, multiplier: 1, constant: -margin)
        
        NSLayoutConstraint.activate([leading,trailing,top,bottom])
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func constraintToWindow() {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.addSubview(self)
        
        let leading = NSLayoutConstraint.init(item: self, attribute: .leading, relatedBy: .equal, toItem: window, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint.init(item: self, attribute: .trailing, relatedBy: .equal, toItem: window, attribute: .trailing, multiplier: 1, constant: 0)
        let top = NSLayoutConstraint.init(item: self, attribute: .top, relatedBy: .equal, toItem: window, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint.init(item: self, attribute: .bottom, relatedBy: .equal, toItem: window, attribute: .bottom, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([leading,trailing,top,bottom])
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}

public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad7,11", "iPad7,12":                    return "iPad 7"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad Mini 5"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
