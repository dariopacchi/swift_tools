//
//  BtleHandler.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 10/03/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation
import CoreBluetooth

class BtleHandler : NSObject, CBCentralManagerDelegate, CBPeripheralDelegate{
    
    static let shared = BtleHandler()
    public var autoReconnect = true
    public var onDeviceConnected : ((BtleDevice) -> Void)?
    public var onBtStatusChange : ((CBManagerState, String) -> Void)?
    
    private var registeredDevices = [BtleDevice]()
    private var centralManager: CBCentralManager?
    
    // MARK: - Public Functions
    
    public func startScanning () {
        let centralQueue: DispatchQueue = DispatchQueue(label: Bundle.main.bundleIdentifier!, attributes: .concurrent)
        centralManager = CBCentralManager(delegate: self, queue: centralQueue)
        centralManager?.scanForPeripherals(withServices: nil, options: nil)
    }
    
    public func stopScanning () {
        centralManager?.stopScan()
    }
    
    public func registerNewDevice (device : BtleDevice) {
        registeredDevices.append(device)
    }
    
    public func unregisterDevice (device : BtleDevice) {
        registeredDevices = registeredDevices.filter({ $0.self !== device })
    }
    
    public func unregisterAllDevices () {
        registeredDevices.removeAll()
    }
    
    // MARK: - CBCentralManagerDelegate methods
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        var string = ""
        
        switch central.state {
            
        case .unknown:
            string = "Bluetooth status is UNKNOWN"
        case .resetting:
            string = "Bluetooth status is RESETTING"
        case .unsupported:
            string = "Bluetooth status is UNSUPPORTED"
        case .unauthorized:
            string = "Bluetooth status is UNAUTHORIZED"
        case .poweredOff:
            string = "Bluetooth status is POWERED OFF"
        case .poweredOn:
            string = "Bluetooth status is POWERED ON"
            
            if (autoReconnect) {
                centralManager?.scanForPeripherals(withServices: nil, options: nil)
            }
            
        @unknown default: break
            
        }
        
        onBtStatusChange?(central.state,string)
    }
    
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        guard let device = (registeredDevices.filter{$0.deviceName == peripheral.name}.first) else {
            return
        }
        
        device.peripheral = peripheral
        
        peripheral.delegate = self
        centralManager?.stopScan()
        centralManager?.connect(peripheral)
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        guard let device = (registeredDevices.filter{$0.deviceName == peripheral.name}.first) else {
            return
        }
        device.peripheral = peripheral
        device.connected = true
        peripheral.discoverServices(nil)
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        guard let device = (registeredDevices.filter{$0.deviceName == peripheral.name}.first) else {
            return
        }
        device.peripheral = nil
        device.connected = false
    }
    
    // MARK: - CBPeripheralDelegate methods
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        guard let device = (registeredDevices.filter{$0.deviceName == peripheral.name}.first) else {
            return
        }
        
        for characteristic in service.characteristics! {
            peripheral.setNotifyValue(true, for: characteristic)
            
            guard let bytes = device.sendWhenConnected else {
                return
            }
            let writeData =  Data(bytes)
            peripheral.writeValue(writeData, for: characteristic, type: CBCharacteristicWriteType.withResponse)
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        guard let device = (registeredDevices.filter{$0.deviceName == peripheral.name}.first) else {
            return
        }
        device.onValueChange?([UInt8](characteristic.value!), characteristic)
    }
}
