//
//  swift_toolsTests.swift
//  swift-toolsTests
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import XCTest
@testable import swift_tools_demo

class networkinghandler_tests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGet(){
        
        let expectation = self.expectation(description: "[GET] - Starting get request test")
        
        let request = NetworkingHandlerRequest.init(url: "https://jsonplaceholder.typicode.com/todos/1")
        NetworkingHandler.get(handlerRequest: request) {[weak self] (response : NetworkingHandlerResponse?, error: Error?) in
            guard error == nil else {
                XCTAssert(response?.statusCode == 200 , "[GET] - Status code should be 200 \(error.debugDescription)")
                return
            }
            self?.printLog(text: "[GET] Response: \n\(response?.dataInString() ?? "")")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 20, handler: nil)
    }

    func testPost(){
        
        let expectation = self.expectation(description: "[POST] - Starting post request test")
        
        let params = [
          "title": "foo",
          "body": "bar",
          "userId" : 1
            ] as [String : Any]
        
        let request = NetworkingHandlerRequest.init(url: "https://jsonplaceholder.typicode.com/posts", params: params)
        NetworkingHandler.post(handlerRequest: request) {[weak self] (response : NetworkingHandlerResponse?, error: Error?) in
            guard error == nil else {
                XCTAssert(response?.statusCode == 200 , "[POST] - Status code should be 200 \(error.debugDescription)")
                return
            }
            self?.printLog(text: "[POST] Response: \n\(response?.dataInString() ?? ""))")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 20, handler: nil)
    }
    
    func testBasicAuthentication(){
        
        let expectation = self.expectation(description: "[BASIC AUTHENTICATION] - Starting post request test")
        
        
        let request = NetworkingHandlerRequest.init(url: "https://jsonplaceholder.typicode.com/posts", username: "user", password: "password")
        NetworkingHandler.basicAuthentication(handlerRequest: request) {[weak self] (response : NetworkingHandlerResponse?, error: Error?) in
            guard error == nil else {
                XCTAssert(response?.statusCode == 200 , "[BASIC AUTHENTICATION] - Status code should be 200 \(error.debugDescription)")
                return
            }
            self?.printLog(text: "[BASIC AUTHENTICATION] Response: \n\(response?.dataInString() ?? ""))")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 20, handler: nil)
    }
    
    func testMultipart(){
        
        let expectation = self.expectation(description: "[POST] - Starting post request test")
        
        let params = [
          "title": "foo",
          "body": "bar",
          "userId" : 1
            ] as [String : Any]
        
        let request = NetworkingHandlerRequest.init(url: "https://acea-orchestrator.overit.it/italgasCantieri/rest/photoUpload", filename: "archive.zip", data: Data(), mimeType: "application/zip", params: params)

        NetworkingHandler.multipartForm(handlerRequest: request) {[weak self] (response : NetworkingHandlerResponse?, error: Error?) in
            guard error == nil else {
                XCTAssert(response?.statusCode == 200 , "[POST] - Status code should be 200 \(error.debugDescription)")
                return
            }
            self?.printLog(text: "[POST] Response: \n\(response?.dataInString() ?? ""))")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 20, handler: nil)
    }
    
    func testJailbroken () {
        
        XCTAssert(!NetworkingHandler.checkNOTJailbrokenDevice(), "[JAILBREAK] - Device is jailbroken!")
    }
    
    func testOpenUrlInSafari () {
        
        XCTAssert(NetworkingHandler.openUrlWithSafari(url: "https://google.com") , "[OPEN URL] - Can't open url in safari!")
    }

}
