//
//  fileshandler_tests.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 10/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import XCTest
@testable import swift_tools_demo

class fileshandler_tests : XCTestCase {
    
    private let filename = "testfile.dae"
    private let folder = "testfolder"
    private var testBundle : Bundle?

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        testBundle = Bundle(for: type(of: self))
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testReadFileFromBundle () {
        XCTAssert(FilesHandler.readFileFromBundle(filename: filename, bundle: testBundle) != nil, "[FilesHandler] - Test Failed!")
    }
    
    func testWriteFile () {
        let file = FilesHandler.readFileFromBundle(filename: filename, bundle: testBundle)!
        XCTAssert(FilesHandler.writeFile(filename: filename, folder: folder, data: file), "[FilesHandler] - Test Failed!")
    }
    
    func testFileExist () {
        XCTAssert(FilesHandler.exist(filename: filename, folder: folder), "[FilesHandler] - Test Failed!")
    }
    
    func testReadFile () {
        XCTAssert((FilesHandler.readFile(filename: filename, folder: folder) != nil), "[FilesHandler] - Test Failed!")
    }
    
    func testSizeOfFolder () {
        let size = FilesHandler.sizeOf(folder: folder)
        printLog(text: size.string)
        XCTAssertGreaterThan(size.size, 0, "[FilesHandler] - Test Failed!")
    }
    
    func testSizeOfFile () {
        let size = FilesHandler.sizeOf(filename: filename, folder: folder)
        printLog(text: size.string)
        XCTAssertGreaterThan(size.size, 0, "[FilesHandler] - Test Failed!")
    }
    
//    func testDelete () {
//        XCTAssert(FilesHandler.delete(filename: filename, folder: folder), "[FilesHandler] - Test Failed!")
//    }
}
