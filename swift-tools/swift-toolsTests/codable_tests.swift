//
//  codable_tests.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 13/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import XCTest
@testable import swift_tools_demo

class codable_tests : XCTestCase {
    
    func testCodable (){
        
        let structure = TestCodableObject(title: "Root", index: 0 ,children: [])
        recursivelyMakeCodableStructure(parent: structure, numberOfChildrens: 10, levels: 3)
        
        do {
            //Test encoding
            XCTAssertNotNil(try structure.encoded())
            
            //Test encoding to file
            XCTAssert(structure.encodeToFile(filename: "Root.json", folder: "Jsons"))
            
            //Test decoding from file
            let root = decodeFromFile(filename: "Root.json", folder: "Jsons") as TestCodableObject?
            XCTAssertNotNil(root)
            
            //Test print
            try printLog(text: root?.encodedToString())
            
        } catch {
           
        }
    }
    
    func testCodableArray (){
        
        let a = TestCodableObject(title: "Root", index: 0 ,children: [])
        let b = TestCodableObject(title: "Root", index: 0 ,children: [])

        let structure = [a,b]
        
        do {
            //Test encoding
            XCTAssertNotNil(try structure.encoded())
            
            //Test encoding to file
            XCTAssert(structure.encodeToFile(filename: "Array.json", folder: "Jsons"))
            
            //Test decoding from file
            let root = decodeFromFile(filename: "Array.json", folder: "Jsons") as [TestCodableObject]?
            XCTAssertNotNil(root)
            
            //Test print
            try printLog(text: root?.encodedToString())
            
        } catch {
           
        }
    }
    
    func recursivelyMakeCodableStructure(parent: TestCodableObject?, numberOfChildrens: Int, levels : Int) {
        
        guard levels > 0 else {
            return
        }
        
        guard parent != nil else {
            return
        }
        
        parent?.children = Array<TestCodableObject>()
        for i in 1...numberOfChildrens {
            
            let testCodableObject = TestCodableObject(title: "Object number \(i) of \(numberOfChildrens)children - Level: \(levels)",index: i)
            parent?.children.append(testCodableObject)
            recursivelyMakeCodableStructure(parent: testCodableObject, numberOfChildrens: numberOfChildrens, levels: levels-1)
        }
    }
}
