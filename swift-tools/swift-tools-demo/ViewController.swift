//
//  ViewController.swift
//  swift-tools
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    private let filename = "testfile.dae"
    private let folder = "testfolder"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        LocationHandler.shared.setOnPermissionChange {[weak self]  (success : Bool) in
//            self?.printLog(text: "Permission: \(success)")
//        }
//       
//        LocationHandler.shared.setOnLocationChange {[weak self]  (location : CLLocation?) in
//            self?.printLog(text: "location: \(location ?? CLLocation())")
//        }
//        
//        LocationHandler.shared.setOnHeadingChange {[weak self]  (heading : CLHeading) in
//            self?.printLog(text: "heading: \(heading)")
//        }
        
//        LocationHandler.shared.requestGPS()
//        LocationHandler.shared.enableGps()
        
//        var date = Date()
//        
//        let mediumDateString = date.toString(style: .medium)
//        let rssDateString = date.toString(format: .rss)
//        let shortTimeString =  date.toString(dateStyle: .none, timeStyle: .short)
//        let relativeTimeSting = date.toStringWithRelativeTime()
//        
//        let twoHoursBefore = date.adjust(.hour, offset: -2)
//        let atNoon = date.adjust(hour: 12, minute: 0, second: 0)
//        
//        printLog(text: atNoon.toString(format: .custom("EEEE MMMM dd/MM/yyyy - HH:mm"), locale: Locale(identifier: "it_IT")))
//        
//        let isToday = date.compare(.isToday)
//        let isSameWeek = date.compare(.isSameWeek(as: twoHoursBefore))
        
//        PushNotificationsHandler.register { (success: Bool) in
//            guard success == true else {
//                return
//            }
//
//            PushNotificationsHandler.enable {[weak self] (success : Bool)  in
//                self?.printLog(text: "Push notifications: \(success == true ? "Enabled":"Disabled")")
//            }
//        }
//
//        NotificationCenter.default.addObserver(forName:PushNotificationsHandler.PushNotificationName, object: nil, queue: nil) {[weak self] (notification : Notification) in
//            self?.printLog(text: notification.userInfo)
//        }
        
//        let v = UIView.init()
//        v.backgroundColor = UIColor.red
//        v.constraintInView(superView: self.view, margin : 50)
  
        
       
        
//        v.constraintInView(superView: self.view, margin : 50)
        
//        testCodable()
                
        //Setup each Btle Device
        let btleDevice = BtleDevice(deviceName: "DIAMOND CUFF BP", readCharacteristic: "00001523-1212-EFDE-1523-785FEABCD123", writeCharacteristic: "00001523-1212-EFDE-1523-785FEABCD123", onValueChange: {bytes, characteristic in
            
            var string = ""
            //Do something with values
            if (bytes[0] == 81 && bytes[1] == 67) {
                
                //Create objects
                let sys = Measurement.init(measurementHubDate: Date(), measurementDeviceDate: Date(), measurementType: 2, measurementValue: Float(bytes[3]))
                let dia = Measurement.init(measurementHubDate: Date(), measurementDeviceDate: Date(), measurementType: 33, measurementValue: Float(bytes[4]))
                let pul = Measurement.init(measurementHubDate: Date(), measurementDeviceDate: Date(), measurementType: 27, measurementValue: Float(bytes[5]))

                sys.save()
                dia.save()
                pul.save()
                
                string = "SYS : \(bytes[3]), DIA : \(bytes[4]), pul : \(bytes[5])"
            } else if (bytes[0] == 81 && bytes[1] == 71) {
                string = "mmHG : \(bytes[2])"
            }
            print(string)
        })
        btleDevice.sendWhenConnected = [0x51, 0x43, 0x00, 0x00, 0x01, 0x01, 0xA3, 0x39]
       
        
        //Setup Btle Handler
        BtleHandler.shared.registerNewDevice(device: btleDevice)
        BtleHandler.shared.onDeviceConnected = {device in
            print("Connected to device: \(device.deviceName)")
        }
        BtleHandler.shared.onBtStatusChange = {status, string in
            print("Bluetooth status: \(string)")
        }
        BtleHandler.shared.startScanning()
      
    }
    
    @IBAction func buttonClick(_ sender: UIButton) {
        
        let v = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
           v.backgroundColor = UIColor.red
           self.view.addSubview(v)
        v.addDrag()
        v.addGravity()
//        
//        let _ = PushNotificationsBanner.presentBanner(title: "Test", subtitle: "This is a test banner", icon: nil, onTap: {
//            self.printLog(text: "TAPPED")
//        })
        
//        presentBoolAlert(withTitle: "Alert", text: "present another alert?", buttons: (ok: "Yes hit me", ko: "Naah")) { [weak self] (ok : Bool) in
//            if (ok) {
//                self?.presentAlert(withTitle: "New alert", text: "nw with a simple choice", buttons: ["Choice 1", "Choice 2", "Choice 3"]) {[weak self] (index : Int) in
//                    self?.presentSimpleAlert(withTitle: "Good choice!", text: "\(index+1) was definitely the right one!", buttonText: "Ok", preferredStyle: .actionSheet, anchor: sender)
//                }
//            }
//        }
    }

    func testCodable (){
        
        printLog(text: FilesHandler.getDocumentsDirectory().absoluteString)
        
        let a = TestCodableObject(title: "Root", index: 0)
        let b = TestCodableObject(title: "Root", index: 0)

        let structure = TestCodableObject(title: "Root", index: 0)
        recursivelyMakeCodableStructure(parent: structure, numberOfChildrens: 10, levels: 3)
        
        do {
            //Test encoding
            let data = try structure.encoded()
            
            //Test encoding to file
            let bool = structure.encodeToFile(filename: "Root.json", folder: "Jsons")
            
            //Test decoding from file
            let data2 = decodeFromFile(filename: "Root.json", folder: "Jsons") as TestCodableObject?
            
            //Test print
            try printLog(text: data2?.encodedToString())
            
        } catch {
           
        }
        
        
        
    }
    
    func recursivelyMakeCodableStructure(parent: TestCodableObject?, numberOfChildrens: Int, levels : Int) {
        
        guard levels > 0 else {
            return
        }
        
        guard parent != nil else {
            return
        }
        
        parent?.children = Array<TestCodableObject>()
        for i in 1...numberOfChildrens {
            
            let testCodableObject = TestCodableObject(title: "Object number \(i) of \(numberOfChildrens)children - Level: \(levels)",index: i)
            parent?.children.append(testCodableObject)
            recursivelyMakeCodableStructure(parent: testCodableObject, numberOfChildrens: numberOfChildrens, levels: levels-1)
        }
    }
    
}

