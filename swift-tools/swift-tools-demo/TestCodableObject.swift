//
//  TestCodableObject.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 13/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation

class TestCodableObject : Codable {
    
    let title : String
    let index : Int
    var children : [TestCodableObject]
    
    init(title: String, index: Int, children:[TestCodableObject]) {
        self.title = title
        self.index = index
        self.children = children
    }
    
    init(title: String, index: Int) {
        self.title = title
        self.index = index
        self.children = []
    }
}
