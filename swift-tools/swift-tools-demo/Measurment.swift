//
//  Measurment.swift
//  swift-tools-demo
//
//  Created by Dario Pacchi on 10/03/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import Foundation

class Measurement : Codable {
    
    let measurementHubDate : String
    let measurementDeviceDate : String
    let measurementType : Int
    let measurementValue : Float
    
    init(measurementHubDate: Date, measurementDeviceDate: Date, measurementType : Int, measurementValue : Float) {
        self.measurementHubDate = "\(measurementHubDate.timeIntervalSince1970)"
        self.measurementDeviceDate = "\(measurementDeviceDate.timeIntervalSince1970)"
        self.measurementType = measurementType
        self.measurementValue = measurementValue
    }
    
    public func save () {
       _ = encodeToFile(filename: "\(measurementHubDate)_\(measurementType)", folder: "Measurements")
    }
}
