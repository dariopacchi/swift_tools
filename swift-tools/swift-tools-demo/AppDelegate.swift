//
//  AppDelegate.swift
//  swift-tools
//
//  Created by Dario Pacchi on 06/02/2020.
//  Copyright © 2020 Dario Pacchi. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseMessaging
import BackgroundTasks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , MessagingDelegate, UNUserNotificationCenterDelegate{
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        PushNotificationsHandler.setup(useBanner: true, defaultIcon: "swift-og")
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        
        return true
    }
        
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
  
        let fileUrls = FilesHandler.getFileUrls(folder: "Measurements")
        var measurements = [Measurement]()
        for url in fileUrls {
            guard let measurement = decodeFromFile(url: url as! URL) as Measurement? else {continue}
            measurements.append(measurement)
        }
        
        print("Measurements count: \(measurements.count)")
        
        let request = NetworkingHandlerRequest.init(url: "https://jsonplaceholder.typicode.com/todos/1")
        NetworkingHandler.get(handlerRequest: request) {(response : NetworkingHandlerResponse?, error: Error?) in
            guard error == nil else {
                return
            }
//            let test = Measurement.init(measurementHubDate: Date(), measurementDeviceDate: Date(), measurementType: 2, measurementValue: 33)
//            test.save()
            completionHandler(.newData)
        }
        
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "swift_tools")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK: - Firebase
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    //
    //    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //      let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
    //      let token = tokenParts.joined()
    //      print("Device Token: \(token)")
    //    }
    //
    //    func application(_ application: UIApplication,didFailToRegisterForRemoteNotificationsWithError error: Error) {
    //      print("Failed to register: \(error)")
    //    }
    
}

